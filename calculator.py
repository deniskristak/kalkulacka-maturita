from tkinter import *
from numpy import sqrt, deg2rad, sin
temp_var = None


def quad_equation(event):
    global A_par_entry, B_par_entry, C_par_entry, print_sol
    root2 = Tk()

    A_par_entry = Entry(root2)
    A_par_entry.grid(row=2, column=1)
    B_par_entry = Entry(root2)
    B_par_entry.grid(row=3, column=1)
    C_par_entry = Entry(root2)
    C_par_entry.grid(row=4, column=1)

    solEntry = Entry(root2, width=30)
    solEntry.grid(row=7, column=0)

    def eq_count(event):
        global A_par_entry, B_par_entry, C_par_entry, print_sol
        A = A_par_entry.get()
        B = B_par_entry.get()
        C = C_par_entry.get()
        D = int(B) ** 2 - 4 * int(A) * int(C)
        if D == 0:
            x = -int(B) / (2 * int(A))
            solEntry.delete(0, 'end')
            solEntry.insert(0, 'x = {0}'.format(x))
        elif D > 0:
            x1 = (-(int(B)) + sqrt(int(D))) / (2 * int(A))
            x2 = (-(int(B)) - sqrt(int(D))) / (2 * int(A))
            solEntry.delete(0, 'end')
            solEntry.insert(0, 'x1 = {}, x2 = {}'.format(x1, x2))
        elif D < 0:
            solEntry.delete(0, 'end')
            solEntry.insert(0, 'Equation has no solution')


    Label(root2, text='Calculate roots of a quadratic equation').grid(row=0, column=0)
    spacer1 = Label(root2, text='\n').grid(row=1, column=0)
    spacer2 = Label(root2, text='\n').grid(row=1, column=1)
    spacer3 = Label(root2, text='\n').grid(row=5, column=1)
    A_par_lab = Label(root2, text='Enter parameter A: ').grid(row=2, column=0)
    B_par_lab = Label(root2, text='Enter parameter B: ').grid(row=3, column=0)
    C_par_lab = Label(root2, text='Enter parameter C: ').grid(row=4, column=0)
    solution_lab = Label(root2, text='Solution:').grid(row=6, column=0)
    
    Count = Button(root2, text='CALCULATE')
    Count.grid(row=5, column=1)
    Count.bind('<Button-1>', eq_count)

    root2.mainloop()


def last_step(event):
    global temp_var, second_num, result
    second_num = ENTRY.get()
    ENTRY.delete(0, 'end')
    if temp_var == '+':
        result = int(first_num) + int(second_num)
    elif temp_var == '-':
        result = int(first_num) - int(second_num)
    elif temp_var == '*':
        result = int(first_num) * int(second_num)
    elif temp_var == '/':
        result = int(first_num) / int(second_num)
    elif temp_var == '%':
        result = int(first_num) % int(second_num)
    ENTRY.insert(0, result)

def make_sum(event):
    global temp_var
    global first_num
    first_num = ENTRY.get()
    ENTRY.delete(0, 'end')
    temp_var = '+'


def make_diff(event):
    global temp_var
    global first_num
    first_num = ENTRY.get()
    ENTRY.delete(0, 'end')
    temp_var = '-'


def make_mult(event):
    global temp_var
    global first_num
    first_num = ENTRY.get()
    ENTRY.delete(0, 'end')
    temp_var = '*'


def make_div(event):
    global temp_var
    global first_num
    first_num = ENTRY.get()
    ENTRY.delete(0, 'end')
    temp_var = '/'


def make_pwr(event):
    num = ENTRY.get()
    ENTRY.delete(0, 'end')
    ENTRY.insert(0, int(num)**2)


def make_sqrt(event):
    num = ENTRY.get()
    ENTRY.delete(0, 'end')
    ENTRY.insert(0, sqrt(int(num)))


def make_mod(event):
    global temp_var
    global first_num
    first_num = ENTRY.get()
    ENTRY.delete(0, 'end')
    temp_var = '%'


def conversionsroot(event):
    root1 = Tk()
    root1.title('Conversions')

    def cel_fah(event):
        res = int(entry_input.get()) * 1.8 + 32
        entry_output.delete(0, 'end')
        entry_output.insert(0, res)

    def fah_cel(event):
        res = (int(entry_input.get()) - 32) * (5 / 9)
        entry_output.delete(0, 'end')
        entry_output.insert(0, res)

    def kel_cel(event):
        res = int(entry_input.get()) - 273.15
        entry_output.delete(0, 'end')
        entry_output.insert(0, res)

    def cel_kel(event):
        res = int(entry_input.get()) + 273.15
        entry_output.delete(0, 'end')
        entry_output.insert(0, res)

    def kel_fah(event):
        res = (int(entry_input.get()) * 1.8) - 459.67
        entry_output.delete(0, 'end')
        entry_output.insert(0, res)

    def fah_kel(event):
        res = (int(entry_input.get()) + 459.67) * (5 / 9)
        entry_output.delete(0, 'end')
        entry_output.insert(0, res)

    def deg_rad(event):
        entry_output.delete(0, 'end')
        entry_output.insert(0, deg2rad(int(entry_input.get())))

    def deg_sin(event):
        entry_output.delete(0, 'end')
        entry_output.insert(0, sin(deg2rad(int(entry_input.get()))))

    entry_input = Entry(root1)
    entry_input.grid(row=3, column=0)

    entry_output = Entry(root1)
    entry_output.grid(row=3, column=3)

    to_rad = Button(root1, text='DEGREES TO RADIANS', width=25)
    to_sin = Button(root1, text='SINUS OF AN ANGLE', width=25)
    celsius_to_fahrenheit = Button(root1, text='CELSIUS TO FAHRENHEIT', width=25)
    fahrenheit_to_celsius = Button(root1, text='FAHRENHEIT TO CELSIUS', width=25)
    kelvin_to_celsius = Button(root1, text='KELVIN TO CELSIUS', width=25)
    celsius_to_kelvin = Button(root1, text='CELSIUS TO KELVIN', width=25)
    kelvin_to_fahrenhein = Button(root1, text='KELVIN TO FAHRENHEIT', width=25)
    fahrenheit_to_kelvin = Button(root1, text='FAHRENHEIT TO KELVIN', width=25)

    to_rad.grid(row=0, column=1)
    to_sin.grid(row=1, column=1)
    celsius_to_fahrenheit.grid(row=2, column=1)
    fahrenheit_to_celsius.grid(row=3, column=1)
    kelvin_to_celsius.grid(row=4, column=1)
    celsius_to_kelvin.grid(row=5, column=1)
    kelvin_to_fahrenhein.grid(row=6, column=1)
    fahrenheit_to_kelvin.grid(row=7, column=1)

    to_rad.bind('<Button-1>', deg_rad)
    to_sin.bind('<Button-1>', deg_sin)
    celsius_to_fahrenheit.bind('<Button-1>', cel_fah)
    fahrenheit_to_celsius.bind('<Button-1>', fah_cel)
    kelvin_to_celsius.bind('<Button-1>', kel_cel)
    celsius_to_kelvin.bind('<Button-1>', cel_kel)
    kelvin_to_fahrenhein.bind('<Button-1>', kel_fah)
    fahrenheit_to_kelvin.bind('<Button-1>', fah_kel)

    root1.mainloop()


root = Tk()
root.title('Calculator')

l0 = Label(root, text='     \n   ')
l0.grid(row=0, column=0)

ENTRY = Entry(root)
ENTRY.grid(row=1, column=0)

PlusBut = Button(root, text='+', height=1, width=4)
PlusBut.grid(row=0, column=1)
PlusBut.bind('<Button-1>', make_sum)

MinusBut = Button(root, text='-', height=1, width=4)
MinusBut.grid(row=1, column=1)
MinusBut.bind('<Button-1>', make_diff)

TimesBut = Button(root, text='*', height=1, width=4)
TimesBut.grid(row=2, column=1)
TimesBut.bind('<Button-1>', make_mult)

DivBut = Button(root, text='/', height=1, width=4)
DivBut.grid(row=3, column=1)
DivBut.bind('<Button-1>', make_div)

PowerBut = Button(root, text='**', height=1, width=4)
PowerBut.grid(row=4, column=0, sticky=E)
PowerBut.bind('<Button-1>', make_pwr)

SqrtBut = Button(root, text='SQRT', height=1, width=4)
SqrtBut.grid(row=4, column=0)
SqrtBut.bind('<Button-1>', make_sqrt)

ModBut = Button(root, text='%', height=1, width=4)
ModBut.grid(row=4, column=0, sticky=W)
ModBut.bind('<Button-1>', make_mod)

EqualBut = Button(root, text='=', height=1, width=4)
EqualBut.grid(row=4, column=1)
EqualBut.bind('<Button-1>', last_step)

l5 = Label(root, text='     \n   ')
l5.grid(row=5, column=0)

Conversions = Button(root, text='CONVERSIONS', height=1, width=18)
Conversions.grid(row=6, column=0, sticky=W)
Conversions.bind('<Button-1>', conversionsroot)

Quadratic_equation = Button(root, text='QUADRATIC EQUATION', height=1, width=18)
Quadratic_equation.grid(row=7, column=0, sticky=W)
Quadratic_equation.bind('<Button-1>', quad_equation)





root.mainloop()

